//
//  StoryBack.m
//  StoryApp
//
//  Created by Mattias k on 2015-02-01.
//  Copyright (c) 2015 Mattias k. All rights reserved.
//

#import "StoryBack.h"

@interface StoryBack()

@property (nonatomic) NSArray *items;
@property (nonatomic) NSArray *cities;
@property (nonatomic) NSArray *victims;
@property (nonatomic) NSArray *feelings;
@property (nonatomic) NSArray *actions;
@property (nonatomic) NSArray *cops;
@property (nonatomic) NSArray *locations;
@property (nonatomic) NSArray *destinations;
@property (nonatomic) NSArray *thiefs;

@end

@implementation StoryBack


- (NSArray *)items {
    if (!_items) {
        _items = @[@"moped", @"traktor", @"helikopter", @"båt", @"cykel", @"segway"];
    }
    return _items;
}
- (NSArray *)cities {
    if (!_cities) {
         _cities = @[@"Långtbortistan", @"Motala", @"Singapore", @"Vetlanda", @"Fjärås", @"Cairns"];
    }
    return _cities;
}
- (NSArray *)victims {
    if (!_victims) {
        _victims = @[@"kungen", @"Brad Pitt", @"Lars Ohly", @"Bert Karlsson", @"Sune", @"Pippi", @"Lady Gaga"];
    }
    return _victims;
}
- (NSArray *)feelings {
    if (!_feelings) {
        _feelings = @[@"rosenrasande", @"jätteglad", @"förbannad", @"gråtfärdig"];
    }
    return _feelings;
}
- (NSArray *)actions {
    if (!_actions) {
        _actions = @[@"slet sitt hår", @"skuttade runt", @"simmade vansbrosimmet", @"gjorde en cover", @"skolade om sig till frisör"];
    }
    return _actions;
}
- (NSArray *)cops {
    if (!_cops) {
        _cops = @[@"kurt w", @"kling & klang", @"gunvald", @"beck", @"späck"];
    }
    return _cops;
}
- (NSArray *)locations {
    if (!_locations) {
        _locations = @[@"semester", @"cafe", @"gymmet", @"kyrkogården", @"bilprovningen"];
    }
    return _locations;
}
- (NSArray *)destinations {
    if (!_destinations) {
        _destinations = @[@"gnesta", @"vasastan", @"rio", @"säffle", @"bankok", @"LA"];
    }
    return _destinations;
}
- (NSArray *)thiefs {
    if (!_thiefs) {
        _thiefs = @[@"pelle svanslös", @"alfons åbergs", @"nalle puhs", @"britney spears", @"silvias"];
    }
    return _thiefs;
}



-(NSString *)randomElement:(NSArray*)array {
    return array[arc4random() % array.count];
}

-(NSString *)finishedStory {
    NSString *item = [self randomElement:[self items]];
    NSString *city = [self randomElement:[self cities]];
    NSString *victim = [self randomElement:[self victims]];
    NSString *feeling = [self randomElement:[self feelings]];
    NSString *action = [self randomElement:[self actions]];
    NSString *cop = [self randomElement:[self cops]];
    NSString *location = [self randomElement:[self locations]];
    NSString *destination = [self randomElement:[self destinations]];
    NSString *thief = [self randomElement:[self thiefs]];
    NSString *summary = [NSString stringWithFormat:@"Under fredagen stals en %@ från %@. Denna %@ visade sig tillhöra %@ som blev %@ och %@. %@ ringde polisen som hette %@ som tyvärr va på %@ i %@ och brottet förblev olöst till tjuven %@ glädje", item, city, item, victim, feeling, action, victim, cop, location, destination, thief];
    return summary;
}

@end
