//
//  StoryBack.h
//  StoryApp
//
//  Created by Mattias k on 2015-02-01.
//  Copyright (c) 2015 Mattias k. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StoryBack : NSObject
-(NSString*)finishedStory;

@end
