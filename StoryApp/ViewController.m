//
//  ViewController.m
//  StoryApp
//
//  Created by Mattias k on 2015-02-01.
//  Copyright (c) 2015 Mattias k. All rights reserved.
//

#import "ViewController.h"
#import "StoryBack.h"

@interface ViewController ()

@property (weak, nonatomic) IBOutlet UITextView *storyText;

@end

@implementation ViewController

- (IBAction)generate:(id)sender {
    StoryBack *story =[[StoryBack alloc]init];
    self.storyText.text = [story finishedStory];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
